//sum
let neoSubFirst = 60;
let neoSubSecond = 57;
let neoSubThird = 72;
let neoSubFourth = 88;
console.log("Neo got in first subject: " + neoSubFirst);
console.log("Neo got in second subject: " + neoSubSecond);
console.log("Neo got in third subject: " + neoSubThird);
console.log("Neo got in fourth subject: " + neoSubFourth);

let indianaSubFirst = 78;
let indianaSubSecond = 52;
let indianaSubThird = 66;
let indianaSubFourth = 80;
console.log();
console.log("Indiana got in first subject: " + indianaSubFirst);
console.log("Indiana got in second subject: " + indianaSubSecond);
console.log("Indiana got in third subject: " + indianaSubThird);
console.log("Indiana got in fourth subject: " + indianaSubFourth);

let severusaSubFirst = 75;
let severusaSubSecond = 67;
let severusaSubThird = 54;
let severusaSubFourth = 90;
console.log();
console.log("Severusa got in first subject: " + severusaSubFirst);
console.log("Severusa got in second subject: " + severusaSubSecond);
console.log("Severusa got in third subject: " + severusaSubThird);
console.log("Severusa got in fourth subject: " + severusaSubFourth);

let aladiniSubFirst = 80;
let aladiniSubSecond = 52;
let aladiniSubThird = 68;
let aladiniSubFourth = 76;
console.log();
console.log("Aladini got in first subject: " + aladiniSubFirst);
console.log("Aladini got in second subject: " + aladiniSubSecond);
console.log("Aladini got in third subject: " + aladiniSubThird);
console.log("Aladini got in fourth subject: " + aladiniSubFourth);

console.log();
console.log();
console.log();
let neoSum = neoSubFirst + neoSubSecond + neoSubThird + neoSubFourth;
console.log("Neo got " + neoSum + " in all subjects together.");
let indianaSum =
  indianaSubFirst + indianaSubSecond + indianaSubThird + indianaSubFourth;
console.log("Indiana got " + indianaSum + " in all subjects together.");
let severusaSum =
  severusaSubFirst + severusaSubSecond + severusaSubThird + severusaSubFourth;
console.log("Severusa got " + severusaSum + " in all subjects together.");
let aladiniSum =
  aladiniSubFirst + aladiniSubSecond + aladiniSubThird + aladiniSubFourth;
console.log("Aladin got " + aladiniSum + " in all subjects together.");

let bestSum;
let bestSumStudent;
if (neoSum >= indianaSum && neoSum >= severusaSum && neoSum >= aladiniSum) {
  bestSum = neoSum;
} else if (
  indianaSum >= neoSum &&
  indianaSum >= severusaSum &&
  indianaSum >= aladiniSum
) {
  bestSum = indianaSum;
} else if (
  severusaSum >= neoSum &&
  severusaSum >= indianaSum &&
  severusaSum >= aladiniSum
) {
  bestSum = severusaSum;
} else if (
  aladiniSum >= neoSum &&
  aladiniSum >= indianaSum &&
  aladiniSum >= severusaSum
) {
  bestSum = aladiniSum;
}

switch (true) {
  case neoSum >= indianaSum && neoSum >= severusaSum && neoSum >= aladiniSum:
    bestSum = neoSum;
    break;
  case indianaSum >= neoSum &&
    indianaSum >= severusaSum &&
    indianaSum >= aladiniSum:
    bestSum = indianaSum;
    break;
  case severusaSum >= neoSum &&
    severusaSum >= indianaSum &&
    severusaSum >= aladiniSum:
    bestSum = severusaSum;
    break;
  case aladiniSum >= neoSum &&
    aladiniSum >= indianaSum &&
    aladiniSum >= severusaSum:
    bestSum = aladiniSum;
    break;
}

if (bestSum == neoSum) {
  bestSumStudent = "Neo";
} else if (bestSum == indianaSum) {
  bestSumStudent = "Indiana";
} else if (bestSum == severusaSum) {
  bestSumStudent = "Severusa";
} else if (bestSum == aladiniSum) {
  bestSumStudent = "Aladini";
}

switch (bestSum) {
  case neoSum:
    bestSumStudent = "Neo";
    break;
  case indianaSum:
    bestSumStudent = "Indiana";
    break;
  case severusaSum:
    bestSumStudent = "Severusa";
    break;
  case aladiniSum:
    bestSumStudent = "Aladini";
    break;
}

console.log();
console.log();
console.log(bestSumStudent + " got the best score: " + bestSum);

// artithmetical average
let neoArthAve = neoSum / 4;
let indianaArthAve = indianaSum / 4;
let severusaArthAve = severusaSum / 4;
let aladiniArthAve = aladiniSum / 4;

console.log();
console.log();
console.log("Neo's arithmetical average is: " + neoArthAve + ".");
console.log("Indiana's arithmetical average is: " + indianaArthAve + ".");
console.log("Severusas's arithmetical average is: " + severusaArthAve + ".");
console.log("Aladin's arithmetical average is: " + aladiniArthAve + ".");

let bestArthAve;
let bestArthAveStudent;

if (
  neoArthAve >= indianaArthAve &&
  neoArthAve >= severusaArthAve &&
  neoArthAve >= aladiniArthAve
) {
  bestArthAve = neoArthAve;
} else if (
  indianaArthAve >= neoArthAve &&
  indianaArthAve >= severusaArthAve &&
  indianaArthAve >= aladiniArthAve
) {
  bestArthAve = indianaArthAve;
} else if (
  severusaArthAve >= neoArthAve &&
  severusaArthAve >= indianaArthAve &&
  severusaArthAve >= aladiniArthAve
) {
  bestArthAve = severusaArthAve;
} else if (
  aladiniArthAve >= neoArthAve &&
  aladiniArthAve >= indianaArthAve &&
  aladiniArthAve >= severusaArthAve
) {
  bestArthAve = aladiniArthAve;
}

switch (true) {
  case neoArthAve >= indianaArthAve &&
    neoArthAve >= severusaArthAve &&
    neoArthAve >= aladiniArthAve:
    bestArthAve = neoArthAve;
    break;
  case indianaArthAve >= neoArthAve &&
    indianaArthAve >= severusaArthAve &&
    indianaArthAve >= aladiniArthAve:
    bestArthAve = indianaArthAve;
    break;
  case severusaArthAve >= neoArthAve &&
    severusaArthAve >= indianaArthAve &&
    severusaArthAve >= aladiniArthAve:
    bestArthAve = severusaArthAve;
    break;
  case aladiniArthAve >= neoArthAve &&
    aladiniArthAve >= indianaArthAve &&
    aladiniArthAve >= severusaArthAve:
    bestArthAve = aladiniArthAve;
    break;
}

if (bestArthAve == neoArthAve) {
  bestArthAveStudent = "Neo";
} else if (bestArthAve == indianaArthAve) {
  bestArthAveStudent = "Indiana";
} else if (bestArthAve == severusaArthAve) {
  bestArthAveStudent = "Severusa";
} else if (bestArthAve == aladiniArthAve) {
  bestArthAveStudent = "Aladini";
}

switch (bestArthAve) {
  case neoArthAve:
    bestArthAveStudent = "Neo";
    break;
  case indianaArthAve:
    bestArthAveStudent = "Indiana";
    break;
  case severusaArthAve:
    bestArthAveStudent = "Severusa";
    break;
  case aladiniArthAve:
    bestArthAveStudent = "Aladini";
    break;
}

console.log();
console.log();
console.log(
  bestArthAveStudent + " got highest arithmetical average: " + bestArthAve + "."
);

//percentage

let neoPerc = (neoArthAve * 100) / 100;
let indianaPerc = (neoArthAve * 100) / 100;
let severusaPerc = (neoArthAve * 100) / 100;
let aladiniPerc = (neoArthAve * 100) / 100;

console.log();
console.log();
console.log("Neo's percentage is: " + neoPerc + "%.");
console.log("Indiana's percentage is: " + indianaPerc + "%.");
console.log("Severusas's percentage is: " + severusaPerc + "%.");
console.log("Aladin's percentage is: " + aladiniPerc + "%.");

let bestPerc;
let bestPercStudent;
if (
  neoPerc >= indianaPerc &&
  neoPerc >= severusaPerc &&
  neoPerc >= aladiniPerc
) {
  bestPerc = neoPerc;
} else if (
  indianaPerc >= neoPerc &&
  indianaPerc >= severusaPerc &&
  indianaPerc >= aladiniPerc
) {
  bestPerc = indianaPerc;
} else if (
  severusaPerc >= neoPerc &&
  severusaPerc >= indianaPerc &&
  severusaPerc >= aladiniPerc
) {
  bestPerc = severusaPerc;
} else if (
  aladiniPerc >= neoPerc &&
  aladiniPerc >= indianaPerc &&
  aladiniPerc >= severusaPerc
) {
  bestPerc = aladiniPerc;
}

switch (true) {
  case neoPerc >= indianaPerc &&
    neoPerc >= severusaPerc &&
    neoPerc >= aladiniPerc:
    bestPerc = neoPerc;
    break;
  case indianaPerc >= neoPerc &&
    indianaPerc >= severusaPerc &&
    indianaPerc >= aladiniPerc:
    bestPerc = indianaPerc;
    break;
  case severusaPerc >= neoPerc &&
    severusaPerc >= indianaPerc &&
    severusaPerc >= aladiniPerc:
    bestPerc = severusaPerc;
    break;
  case aladiniPerc >= neoPerc &&
    aladiniPerc >= indianaPerc &&
    aladiniPerc >= severusaPerc:
    bestPerc = aladiniPerc;
}

if (bestPerc == neoPerc) {
  bestPercStudent = "Neo";
} else if (bestPerc == indianaPerc) {
  bestPercStudent = "Indiana";
} else if (bestPerc == severusaPerc) {
  bestPercStudent = "Severusa";
} else if (bestPerc == aladinArithmeticAverage) {
  bestPercStudent = "Aladini";
}

switch (bestPerc) {
  case neoPerc:
    bestPercStudent = "Neo";
    break;
  case indianaPerc:
    bestPercStudent = "Indiana";
    break;
  case severusaPerc:
    bestPercStudent = "Severusa";
    break;
  case aladinArithmeticAverage:
    bestPercStudent = "Aladini";
    break;
}

console.log();
console.log();
console.log(bestPercStudent + " got highest percentage: " + bestPerc + "%.");

//GPA
const subFirstCredit = 4;
const subSecondCredit = 2;
const subThirdCredit = 7;
const subFourthCredit = 5;
const subCreditSum =
  subFirstCredit + subSecondCredit + subThirdCredit + subFourthCredit;

let neoSubFirstGP = 0.5;
let neoSubSecondtGP = 0.5;
let neoSubThirdGP = 2;
let neoSubFourthGP = 3;

let indianaSubFirstGP = 2;
let indianaSubSecondtGP = 0.5;
let indianaSubThirdGP = 1;
let indianaSubFourthGP = 2;

let severusaSubFirstGP = 2;
let severusaSubSecondtGP = 1;
let severusaSubThirdGP = 0.5;
let severusaSubFourthGP = 3;

let aladiniSubFirstGP = 2;
let aladiniSubSecondtGP = 0.5;
let aladiniSubThirdGP = 1;
let aladiniSubFourthGP = 2;

let neoGPA =
  (neoSubFirstGP * subFirstCredit +
    neoSubSecondtGP * subSecondCredit +
    neoSubThirdGP * subThirdCredit +
    neoSubFourthGP * subFourthCredit) /
  subCreditSum;
let indianaGPA =
  (indianaSubFirstGP * subFirstCredit +
    indianaSubSecondtGP * subSecondCredit +
    indianaSubThirdGP * subThirdCredit +
    indianaSubFourthGP * subFourthCredit) /
  subCreditSum;
let severusaGPA =
  (severusaSubFirstGP * subFirstCredit +
    severusaSubSecondtGP * subSecondCredit +
    severusaSubThirdGP * subThirdCredit +
    severusaSubFourthGP * subFourthCredit) /
  subCreditSum;
let aladiniGPA =
  (aladiniSubFirstGP * subFirstCredit +
    aladiniSubSecondtGP * subSecondCredit +
    aladiniSubThirdGP * subThirdCredit +
    aladiniSubFourthGP * subFourthCredit) /
  subCreditSum;

console.log();
console.log();
console.log();
console.log("Neo's GPA: " + neoGPA + ".");
console.log("Indiana's GPA: " + indianaGPA + ".");
console.log("Severusa's GPA: " + severusaGPA + ".");
console.log("Aladini's GPA: " + aladiniGPA + ".");
